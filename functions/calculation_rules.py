"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

def kpi_price(system: LegoAssembly)->float:
    """
    Calculates the total price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_price (float): Sum of prices of all components in the system in Euro

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_price()")

    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
    return total_price # alternative: sum(c.properties["price [Euro]"] for c in system.get_component_list(-1))

def kpi_delivery_time(system: LegoAssembly)->float:
    """
    Calculates the maximum delivery time of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        max_time (float): Max of time of all components in the system in Days

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")

    delivery_time = 0
    for c in system.get_component_list(-1):
        delivery_time = max(c.properties["delivery time [days]"] for c in system.get_component_list(-1))
    return delivery_time 